const express = require("express");
const cors = require("cors");

const app = express();
app.use(cors("*"));
app.use(express.json());

// add the routers
const routerProduct = require("./routes/movie");
app.use("/movie", routerProduct);

app.listen(4000, "0.0.0.0", () => {
  console.log("movie-server started on port 4000");
});
