const mysql = require("mysql");

const pool = mysql.createPool({
  host: "localhost",

  user: "root",
  password: "root",
  database: "moviedb",
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

module.exports = pool;
