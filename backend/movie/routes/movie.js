const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

router.post("/", (request, response) => {
  const { title, price, description, categoryId } = request.body;
  // Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)
  const query = `
    INSERT INTO movie
      (movie_title, movie_release_date, movie_time,director_name)
    VALUES
      ( ${movie_title}, '${movie_release_date}', ${movie_time}, ${director_name})
  `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/", (request, response) => {
  const query = `
    SELECT movie_id, movie_title, movie_release_date, movie_time, director_name 
    FROM movie
  `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.put("/:movie_id", (request, response) => {
  const { movie_id } = request.params;
  const { movie_title, movie_release_date, movie_time, director_name } =
    request.body;

  const query = `
    UPDATE movie
    SET
    movie_title = '${movie_title}', 
    movie_release_date = ${movie_release_date}, 
    movie_time = '${movie_time}', 
    director_name = ${director_name}
    WHERE
    movie_id = ${movie_id}
  `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.delete("/:movie_id", (request, response) => {
  const { movie_id } = request.params;

  const query = `
    DELETE FROM movie
    WHERE
    movie_id = ${movie_id}
  `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

module.exports = router;
